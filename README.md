# Setup

1. Download PHPStorm https://www.jetbrains.com/phpstorm/
2. (Steps 3 and 4 only apply to osx)
3. Install homebrew http://brew.sh/
4. Install php56 https://github.com/Homebrew/homebrew-php#installation (Make sure to install 56, not 71!)
5. Open JetBrains, select openemr project
6. Open preferences, search for "interpreter", click php in the sidebar
7. Click on the "..." next to interpreter, click the +, select "other local"
8. In the path select /usr/local/bin/php (it should say php version is 7.1)
9. Click save
10. Now, in the top right click on "Edit Configurations"
11. Click the plus sign and select "PHP Built-In Webserver"
12. Set your document root to /Users/your_name/path_to_repo/openemr
13. Click apply
14. Now, when you click on the green arrow to run it, and then open a webrowser and go to localhost:#### (Whatever number you set), it should show the setup options
15. Get mysql:
```
brew install mysql
mysqld --initialize --explicit_defaults_for_timestamp
```
You should see a line: "A temporary password is generated for root@localhost: xxxxzzzzyyyy".
Finally, run:
```
mysql.server start
mysql -u root -h 127.0.0.1 -p
........... #[Enter the password from above]
ALTER USER 'root'@'localhost' IDENTIFIED BY 'new-password-here';
```
You should be able to go through the setup options now, and set up with root and new password.
16. Get a database admin tool http://www.sequelpro.com/
17. In your home directory create a php.ini file
18. Inside, put:
```
short_open_tag = Off
max_execution_time = 60
max_input_time = 90
max_input_vars = 3000
memory_limit = 128M
display_errors = Off
log_errors = On
register_globals = Off
post_max_size = 30M
file_uploads = On
upload_max_filesize = 30M
error_reporting = E_ALL & ~E_NOTICE & ~E_STRICT
date.timezone = "America/Toronto"
```
19. Finally, go back to the phpstorm config for the local webserver
20. In the "Interpreter Options" section put
```
--php-ini /Users/your-username-here/php.ini
```
21. Now, when you run the server it will run with the correct options.

Welcome to [OpenEMR](http://www.open-emr.org/)!!!
=====================

[OpenEMR](http://www.open-emr.org/) is a Free and Open Source electronic health records and
medical practice management application. It is ONC Complete
Ambulatory EHR Certified and it features fully integrated electronic
health records, practice management, scheduling, electronic billing,
internationalization, free support, a vibrant community, and a
[whole lot more](http://www.open-emr.org/wiki/index.php/OpenEMR_Features). It can run on Windows, Linux, Mac OS X, and many
other platforms.

The setup documentation can be found in the INSTALL file and extensive
documentation and forums can be found on the OpenEMR website at:
<http://www.open-emr.org>
